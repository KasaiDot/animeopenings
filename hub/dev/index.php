<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="../CSS/main.css">
	<title>Openings.moe hub</title>
	<meta name=viewport content="width=device-width, initial-scale=1">
</head>
<body>

	<div class="container">

		<h1>
			Developer hub
		</h1>

		<ul class="linklist">
			<li class="link">
				<a href="../"><< Back to main hub</a>
			</li>
		</ul>

		<p>This page contains fun stuff for developers and curious individuals who want to learn or mess with the site.</p>

		<ul class="linklist">
			<li class="link">
				<a href="https://github.com/AniDevTwitter/animeopenings">GitHub</a>
			</li>
			<li class="link">
				<a href="api.php">API</a>
			</li>
		</ul>

		<h2>Server map</h2>

		<img src="files/map.svg" style="width: 100%;box-shadow: 0px 0px 2px #111;"></img>

		<?php
		include_once('../../backend/includes/botnet.html');
		?>

	</div>

</body>
</html>
