<!DOCTYPE html>
<html>
	<head>
		<title>Notable people</title>
		<link rel="stylesheet" type="text/css" href="CSS/markdown.css">
		<meta charset="UTF-8">
		<meta name=viewport content="width=device-width, initial-scale=1">
	</head>
	<body>

		<a href="index.php"><< Back to the hub</a>

		<h1 id="notable-people">Notable people</h1>

		<p>These guys are important. Everything from donators to testers and contributors. I pay money to some of them, some give money or services to me. Everything is here.</p>

		<ul>
			<li><a href="https://twitter.com/QuadPiece/">Quad</a> - I kind of run this whole thing</li>
		</ul>

		<h2 id="general-contributors">"Staff"</h2>

		<ul>
			<li><a href="https://twitter.com/dev_loic">pluesch</a> - Massive donator and supporter, pretty much co-leader at this point</li>
			<li>Yay295 - Encoding <s>slave</s></li>
			<li><a href="https://twitter.com/immanenz">immanenz</a> - Pretty much <a href="http://i.imgur.com/oRuwYya.png">this</a></li>
			<li>Tracreed - Developer and encoder</li>
			<li>theholyduck - Encoding beast</li>
			<li>NiseVoid - Encoder</li>
		</ul>

		<h2 id="companies">General contributors</h2>

		<ul>
			<li><a href="https://twitter.com/UwyBBQ">Uwy</a> - Huge donator and supporter</li>
			<li><a href="https://twitter.com/lazzorx">@lazzorx</a> - Submitted encodes</li>
		</ul>

		<h2 id="companies">Companies</h2>

		<ul>
			<li><a href="https://rage4.com/">Rage4</a> - GeoDNS service, enables me to run servers all around the globe</li>
			<li><a href="https://www.digitalocean.com/">DigitalOcean</a> - Powers our SSD servers</li>
		</ul>

		<h2 id="companies">Notable mentions</h2>

		<ul>
			<li><a href="https://twitter.com/saucenao">Xamayon</a> - He runs <a href="http://saucenao.com/">SauceNAO</a>.</li>
		</ul>

		<h2 id="github-contributors">And of course, all our <a href="https://github.com/AniDevTwitter/animeopenings/graphs/contributors">GitHub contributors</a> &lt;3</h2>

		<?php
		include_once('../backend/includes/botnet.html');
		?>

	</body>
</html>
