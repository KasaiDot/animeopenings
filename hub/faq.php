<!DOCTYPE html>
<html>
	<head>
		<title>F.A.Q</title>
		<link rel="stylesheet" type="text/css" href="CSS/markdown.css">
		<meta charset="UTF-8">
		<meta name=viewport content="width=device-width, initial-scale=1">
	</head>
	<body>

		<a href="index.php"><< Back to the hub</a>

		<h1 id="faq">FAQ</h1>

		<p>(Frequently asked questions)</p>

		<h2 id="there-are-endings-too-my-life-is-a-lie">THERE ARE ENDINGS TOO! MY LIFE IS A LIE</h2>

		<p>Well excuse me, but “openingsandendings.moe” was a bit too long for my taste.</p>

		<h2 id="please-add-x-i-said-add-the-opening-from-x-where-the-fuck-is-the-opening-from-x">PLEASE ADD X! I SAID ADD THE OPENING FROM X!!!! WHERE THE FUCK IS THE OPENING FROM X!!!</h2>

		<p>Whoa whoa cowboy, calm down. Openings will come. When you suggest something it’s added to a list. Openings on this list are completed in a random order so someday your opening/ending will arrive. If it will take 30 minutes or 5 weeks however is something I can’t tell you. And nagging does not help (In fact, it’ll probably make me lower the priority)</p>

		<h2 id="i-cant-wait-for-my-opening-to-arrive-you-work-like-a-snail">I can’t wait for my opening to arrive, you work like a snail.</h2>

		<p>Then <a href="encodes.php">submit an encode yourself</a> or find someone else willing to encode and submit it for you.</p>

		<h2 id="why-wont-the-videos-play">Why won’t the videos play?</h2>

		<p>The most likely reasons are:</p>

		<ul>
		<li>You’re using a bad browser (eg. Internet Exploder, Safari)</li>
		</ul>

		<p>Oh wait, that’s mostly it. To solve it, get a proper browser, such as Firefox or Opera.</p>

		<p>If you believe another issue is causing this, contact @QuadPiece on Twitter and we can try working out a solution.</p>

		<h2 id="i-got-this-really-weird-video">I got this really weird video, what does that mean?</h2>

		<p>There are easter eggs hidden on the site. These will report the title and source as "???", contratulations! You found one!</p>

		<?php
		include_once('../backend/includes/botnet.html');
		?>

	</body>
</html>
