<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="CSS/main.css">
	<title>Openings.moe hub</title>
	<meta name=viewport content="width=device-width, initial-scale=1">
</head>
<body>

	<div class="container">

		<h1>
			Openings.moe hub
		</h1>

		<ul class="linklist">
			<li class="link">
				<a href="../"><< Back to main site</a>
			</li>
		</ul>

		<p>This page contains extra links and content to avoid bloating the menu when viewing a video. More may be added later.</p>

		<ul class="linklist">
			<li class="link">
				<a href="../list">Video list</a>
			</li>
			<li class="link">
				<a href="faq.php">F.A.Q</a>
			</li>
			<li class="link">
				<a href="encodes.php">Submit encodes</a>
			</li>
			<li class="link">
				<a href="dev">Developer hub</a>
			</li>
			<li class="link">
				<a href="../donate">Donate</a>
			</li>
			<li class="link">
				<a href="supporters.php">Supporters</a>
			</li>
		</ul>

		<?php
		include_once('../backend/includes/botnet.html');
		?>

	</div>

</body>
</html>
